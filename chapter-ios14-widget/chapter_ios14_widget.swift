//
//  chapter_ios14_widget.swift
//  chapter-ios14-widget
//
//  Created by Mobile2you on 21/07/20.
//

import WidgetKit
import SwiftUI

// Struct a ser passado para o Timeline.
// Deve conter um Date que será o instante de atualização do widget.
// Pode conter ainda outras informações adicionais.
struct WidgetModel: TimelineEntry {
    var date: Date
    var currentTime: String
}

struct DataProvider: TimelineProvider {
    
    // Estado real do widget em funcionamento. Deve contar a política de atualização do Widget, podendo ser:
    // - Never: Para Widgets estáticos
    // - AtEnd: Atualiza após a execução da última entrada do timeline
    // - After(date): Atualiza após um certo instante de tempo
    public func timeline(with context: Context, completion: @escaping (Timeline<WidgetModel>) -> ()) {
        let date = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss a"
        let time = formatter.string(from: date)
        
        let entryData = WidgetModel(date: date, currentTime: time)

        let refresh = Calendar.current.date(byAdding: .second, value: 1, to: date)!

        let timeLine = Timeline(entries: [entryData], policy: .after(refresh))
        
//        print("now: ", date, "\nrefresh: ", refresh, "\n" )
        completion(timeLine)
        
        WidgetCenter.shared.reloadAllTimelines()
    }
    
    // Estado default do widget, aparente na tela de escolha de widgets.
    public func snapshot(with context: Context, completion: @escaping (WidgetModel) -> ()) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss a"
        let time = formatter.string(from: date)
        let entryData = WidgetModel(date: date, currentTime: time)
        completion(entryData)
    }
}

struct WidgetView: View {
    var data: DataProvider.Entry
    @Environment(\.widgetFamily) var family
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text("Relógio M2Y")
                    .font(family == .systemSmall ? .body : (family == .systemMedium ? .title : .largeTitle))
                    .fontWeight(.bold)
                Spacer()
            }
            .padding(.all)
            .background(Color(red: 16/255, green: 77/255, blue: 232/255))
            Spacer()
            Text(data.currentTime)
                .font(family == .systemSmall ? .body : (family == .systemMedium ? .title : .largeTitle))
                .padding(.horizontal, 15)
                .foregroundColor(.white)
            Spacer()
        }
        .background(Color(red: 16/255, green: 77/255, blue: 232/255, opacity: 0.8))
    }
}

struct Placeholder: View {
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text("Relógio M2Y")
                Spacer()
            }
            Spacer()
            Text("Carregando")
                .padding(.horizontal, 15)
            Spacer()
        }
        .background(Color.black)
    }
}

// StaticConfiguration: Widgets estáticos, cujos dados não dependem do usuário
// IntentConfiguration: Widgets não estáticos que podem ser alterados por meio dos intents da Siri pelo usuário
@main
struct Config: Widget {
    var body: some WidgetConfiguration {
        StaticConfiguration(kind: "chapter_ios14_widget", provider: DataProvider(), placeholder: Placeholder()) { data in
            WidgetView(data: data)
        }
        .supportedFamilies([.systemSmall, .systemMedium, .systemLarge])
        .description(Text("Widget Relógio M2Y"))
    }
}
